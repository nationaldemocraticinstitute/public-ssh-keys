#!/bin/bash

declare -A DEVTEAM_KEY
DEVTEAM_KEY["ansible"]="https://bitbucket.org/nationaldemocraticinstitute/public-ssh-keys/raw/beeb62a910ac9fb6dd3db08c1442296d148b78f6/ansible"

# Must be run as root
if [ "$(whoami)" != 'root' ]; then
        echo "$0: ERROR: You have no permission to run $0 as non-root user."
        exit 1;
fi

foo=$(which yum | grep yum)
PACKAGE_NEEDS="htop figlet lsof traceroute vim ncdu wget curl telnet multitail lynx mlocate zip unzip tree screen tmux rsync at python sudo "
if [ -z "$foo" ]
	then 
		apt-get -y install $PACKAGE_NEEDS dnsutils openssh-client
	else 
		yum install -y $PACKAGE_NEEDS yum-utils policycoreutils-python bind-utils openssh-clients
fi

for ADDUSER in ${!DEVTEAM_KEY[@]}
do
        # echo ${DEVTEAM_KEY[$ADDUSER]}
        useradd $ADDUSER
        MYHOMESSH="/home/$ADDUSER/.ssh"
        if [ ! -d "$MYHOMESSH" ]; then
                /bin/echo "$0: No $ADDUSER .ssh folder found.  Creating, and putting in authorized keys."
                /bin/mkdir -p $MYHOMESSH
                /bin/chmod 700 $MYHOMESSH
                wget -q ${DEVTEAM_KEY[$ADDUSER]} -O $MYHOMESSH/authorized_keys
                /bin/chmod 600 $MYHOMESSH/authorized_keys
                /bin/chown -R $ADDUSER:$ADDUSER /home/$ADDUSER/
        else
                /bin/echo "$0: $ADDUSER .ssh folder found.  Will not alter any .ssh files for $ADDUSER."
        fi

        MYSUDOERS="$ADDUSER        ALL=(ALL)       NOPASSWD: ALL"
        /bin/echo $MYSUDOERS >> /etc/sudoers.d/10_$ADDUSER
done 

# Rolando, I put these two lines here because it added me as the first admin.
# You might want to put yourself here. I am commenting it out because I'm leaving.
#
# wget https://bitbucket.org/nationaldemocraticinstitute/public-ssh-keys/raw/master/add_grig.bash -O /tmp/add_grig.bash
# bash /tmp/add_grig.bash

rm -r $0
exit 0;
