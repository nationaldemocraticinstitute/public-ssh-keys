#!/bin/bash

# This is to add Arber, developer for the FMC

declare -A DEVTEAM_KEY
DEVTEAM_KEY["arber"]="https://bitbucket.org/nationaldemocraticinstitute/public-ssh-keys/raw/39d6a21df26ee7f5ad3dd76ef541a41e77f0486c/arber"

# Must be run as root
if [ "$(whoami)" != 'root' ]; then
        echo "$0: ERROR: You have no permission to run $0 as non-root user."
        exit 1;
fi

for ADDUSER in ${!DEVTEAM_KEY[@]}
do
	# echo ${DEVTEAM_KEY[$ADDUSER]}
	useradd $ADDUSER
	MYHOMESSH="/home/$ADDUSER/.ssh"
	if [ ! -d "$MYHOMESSH" ]; then
        	/bin/echo "$0: No $ADDUSER .ssh folder found.  Creating, and putting in authorized keys."
        	/bin/mkdir -p $MYHOMESSH
        	/bin/chmod 700 $MYHOMESSH
        	wget -q ${DEVTEAM_KEY[$ADDUSER]} -O $MYHOMESSH/authorized_keys
		/bin/chmod 600 $MYHOMESSH/authorized_keys
		/bin/chown -R $ADDUSER:$ADDUSER /home/$ADDUSER/
	else
        	/bin/echo "$0: $ADDUSER .ssh folder found.  Will not alter any .ssh files for $ADDUSER."
	fi

	MYSUDOERS="$ADDUSER        ALL=(ALL)       NOPASSWD: ALL"
	/bin/echo $MYSUDOERS >> /etc/sudoers.d/10_$ADDUSER
done 

exit 0;