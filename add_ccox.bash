#!/bin/bash

declare -A DEVTEAM_KEY
DEVTEAM_KEY["ccox"]="https://bitbucket.org/nationaldemocraticinstitute/public-ssh-keys/raw/10fe8159c9ea9700fb3071d2a74f77ed742a1398/carloscox"

# Must be run as root
if [ "$(whoami)" != 'root' ]; then
        echo "$0: ERROR: You have no permission to run $0 as non-root user."
        exit 1;
fi

for ADDUSER in ${!DEVTEAM_KEY[@]}
do
        # echo ${DEVTEAM_KEY[$ADDUSER]}
        useradd $ADDUSER
        MYHOMESSH="/home/$ADDUSER/.ssh"
        if [ ! -d "$MYHOMESSH" ]; then
                /bin/echo "$0: No $ADDUSER .ssh folder found.  Creating, and putting in authorized keys."
                /bin/mkdir -p $MYHOMESSH
                /bin/chmod 700 $MYHOMESSH
                wget -q ${DEVTEAM_KEY[$ADDUSER]} -O $MYHOMESSH/authorized_keys
                /bin/chmod 600 $MYHOMESSH/authorized_keys
                /bin/chown -R $ADDUSER:$ADDUSER /home/$ADDUSER/
        else
                /bin/echo "$0: $ADDUSER .ssh folder found.  Will not alter any .ssh files for $ADDUSER."
        fi

        MYSUDOERS="$ADDUSER        ALL=(ALL)       NOPASSWD: ALL"
        /bin/echo $MYSUDOERS >> /etc/sudoers.d/10_$ADDUSER
done 

exit 0;
