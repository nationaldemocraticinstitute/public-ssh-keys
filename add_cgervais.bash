#!/bin/bash

declare -A DEVTEAM_KEY
DEVTEAM_KEY["cgervais "]="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAmPj8cTqkboLQoOPUdCsQHgpHKlRMtXJqNt4naWYgrznfjGBcm+3F1kWvhl7gnvJjYiikgNxUuwUnaweKFvhn24pu+oFETG9g0ok79zFORqAPYyx1ohkQdIfzmnMRxxnvgiRyhRTiOqUFyYSKGFHI3MaOs7bL20YJo1z+PWsCDmXonUBHvr9law3m9cK+r2dF0YnA2ubZUvT1hfJFqs6QoS/+MBSoPS/ZhMW75KFCJ6/eDkBuXCr0zavm5KGvLWeLq8rQ7U+S5vPdGvRjirK69ZYegEjha1Fr+85NFLlNKCPmcngEhNnUhALDeUaC2DYNqqsRBpqYEDg/YTaU/jl4Yw== socrates32@socrates32-laptop"

# Must be run as root
if [ "$(whoami)" != 'root' ]; then
        echo "$0: ERROR: You have no permission to run $0 as non-root user."
        exit 1;
fi

for ADDUSER in ${!DEVTEAM_KEY[@]}
do
        # echo ${DEVTEAM_KEY[$ADDUSER]}
        useradd $ADDUSER
        MYHOMESSH="/home/$ADDUSER/.ssh"
        if [ ! -d "$MYHOMESSH" ]; then
                /bin/echo "$0: No $ADDUSER .ssh folder found.  Creating, and putting in authorized keys."
                /bin/mkdir -p $MYHOMESSH
                /bin/chmod 700 $MYHOMESSH
                # wget -q ${DEVTEAM_KEY[$ADDUSER]} -O $MYHOMESSH/authorized_keys
                /bin/echo "${DEVTEAM_KEY[$ADDUSER]}" > $MYHOMESSH/authorized_keys
                /bin/chmod 600 $MYHOMESSH/authorized_keys
                /bin/chown -R $ADDUSER:$ADDUSER /home/$ADDUSER/
        else
                /bin/echo "$0: $ADDUSER .ssh folder found.  Will not alter any .ssh files for $ADDUSER."
        fi

        MYSUDOERS="$ADDUSER        ALL=(ALL)       NOPASSWD: ALL"
        /bin/echo $MYSUDOERS >> /etc/sudoers.d/10_$ADDUSER
done 

exit 0;